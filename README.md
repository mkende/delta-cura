# μDelta configuration for Cura

This is a configuration file to use the μDelta (micro-delta) 3D Printers from
eMotion Tech([Reprap France](http://www.reprap-france.com)) with the Cura slicer.

This configuration is meant for the new version of Cura (15.16 onward),
currently tagged as BETA.

## Installation

Just copy the [`micro_delta.json`](https://bitbucket.org/mkende/delta-cura/raw/master/micro_delta.json)
file in the `resources\settings` folder inside Cura's installation dir,
restart Cura, and add a new printer of type "MicroDelta".

## Links
  * [The μDelta on the eMotion Tech website](http://www.reprap-france.com/produit/1234568322-imprimante-3d-micro-delta-revision-1-1).
  * [The Cura homepage](https://ultimaker.com/en/products/cura-software).